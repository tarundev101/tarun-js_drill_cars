// problem 1 answer 
function problem_1(inventory){
    let result='Car 33 is a ';
    
    for (let index = 0;index < inventory.length; index++ ){
        if (inventory[index].id==33) {
            result+=inventory[index].car_year+' '+inventory[index].car_make+' '+inventory[index].car_model;
        }
    }
    return result;
}

module.exports = {problem_1};
